# esnext-react
React under 30k.
esnext-react is an implementation of React that optimizes for small script size.

It supports the core APIs of React, such as Virtual DOM, intended as a drop-in
replacement for React, when you don't need server-side rendering or support for
IE8.

License: MIT (See LICENSE file for details)