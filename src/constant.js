/**
* 常量
*/
export const CREATE = 'CREATE'
export const REMOVE = 'REMOVE'
export const REORDER = 'REORDER'
export const REPLACE = 'REPLACE'
export const INSERT = 'INSERT'
export const PROPS = 'PROPS'
export const WIDGET = 'WIDGET'
export const DID_MOUNT = 'DID_MOUNT'
export const WILL_UNMOUNT = 'WILL_UNMOUNT'
export const COMPONENT_ID = 'data-esnextid'
